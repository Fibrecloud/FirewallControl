[Void][System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")
[Void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

Add-Type -Name "Window" -Namespace "Console" -MemberDefinition '[DllImport("Kernel32.dll")] public static extern IntPtr GetConsoleWindow();[DllImport("user32.dll")] public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int W, int H);'
[Console.Window]::MoveWindow([Console.Window]::GetConsoleWindow(), ([System.Windows.Forms.SystemInformation]::PrimaryMonitorSize.Width), ([System.Windows.Forms.SystemInformation]::PrimaryMonitorSize.Height), 0, 0)

$PowerShell = (Get-Process | Where-Object {$_.MainWindowTitle -EQ $WindowTitle}).MainWindowHandle
Add-Type -Member '[DllImport("user32.dll")] public static extern bool ShowWindow(int handle, int state);' -Name "Win" -Namespace "Native"
[Native.Win]::ShowWindow($PowerShell, 0) | Out-Null

$Global:ProcessGUID = ((([System.Guid]::NewGuid()).Guid).ToUpper()).Substring(0,4)
$Global:WindowTitle = "PowerShell: Firewall Control ($ProcessGUID)"
$Host.UI.RawUI.WindowTitle = $WindowTitle

$ErrorActionPreference = "SilentlyContinue"
$OutputEncoding = [Console]::InputEncoding = [Console]::OutputEncoding = New-Object -TypeName "System.Text.UTF8Encoding"

function Create-FirewallGUI {

    $MainForm = New-Object System.Windows.Forms.Form
    $MainForm.Size = New-Object System.Drawing.Size(600, 355)
    $MainForm.Text = "PowerShell: Firewall Control (Initializing GUI)"
    $MainForm.StartPosition = "CenterScreen"
    $MainForm.FormBorderStyle = "FixedDialog"
    $MainForm.MaximizeBox = $False
    $MainForm.Icon = [System.Drawing.Icon]::ExtractAssociatedIcon((Get-Command PowerShell).Source)

    $MainForm.KeyPreview = $True
    $MainForm.Add_KeyDown({

        $KeyPress = $Args[1].KeyCode

        if ($KeyPress -eq [System.Windows.Forms.Keys]::Enter) {

            $SelectedEntries = $ListView.SelectedItems
            if ($SelectedEntries.Count -gt 0) {

                foreach ($ListEntry in $SelectedEntries) {

                    Cycle-Status -ListEntry $ListEntry

                }

            }

        } elseif ($KeyPress -eq [System.Windows.Forms.Keys]::Delete) {

            $SelectedEntries = $ListView.SelectedItems
            if ($SelectedEntries.Count -gt 0) {

                foreach ($ListEntry in $SelectedEntries) {

                    $SelectedFile = $ListEntry.SubItems[3].Text
                    Remove-FirewallRule -ExecutablePath $SelectedFile
                    $ListEntry.SubItems[1].Text = "None"
                    $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Black

                }

            }

        } elseif ($KeyPress -eq [System.Windows.Forms.Keys]::F5) {

            Refresh-ListView

        }

    })

    $ListView = New-Object System.Windows.Forms.ListView
    $ListView.Size = New-Object System.Drawing.Size(575, 280)
    $ListView.Location = New-Object System.Drawing.Point(5, 5)
    $ListView.View = [System.Windows.Forms.View]::Details
    $ListView.FullRowSelect = $True
    $ListView.HeaderStyle = [System.Windows.Forms.ColumnHeaderStyle]::Nonclickable
    $ListView.BorderStyle = [System.Windows.Forms.BorderStyle]::FixedSingle

    $ListView.Add_DoubleClick({
        $SelectedEntries = $ListView.SelectedItems
        if ($SelectedEntries.Count -eq 1) {
            foreach ($ListEntry in $SelectedEntries) {
                Cycle-Status -ListEntry $ListEntry
            }
        }
    })

    $ImageList = New-Object System.Windows.Forms.ImageList
    $ImageList.ImageSize = New-Object System.Drawing.Size(16, 16)
    $ListView.SmallImageList = $ImageList

    $ListView.Columns.Add("Icon", 20)
    $ListView.Columns.Add("Status", 60)
    $ListView.Columns.Add("File Name", 120)
    $ListView.Columns.Add("File Path", 370)
    $ListView.HeaderStyle = [System.Windows.Forms.ColumnHeaderStyle]::None
    $ListView.GridLines = $False
    $ListView.Scrollable = $True
    $ListView.HotTracking = $False

    $AllowButton = New-Object System.Windows.Forms.Button
    $AllowButton.Text = "Allow"
    $AllowButton.Location = New-Object System.Drawing.Point(5, 290)
    $AllowButton.Size = New-Object System.Drawing.Size(75, 25)
    $AllowButton.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
    $AllowButton.FlatAppearance.BorderSize = 1
    $AllowButton.BackColor = [System.Drawing.Color]::FromArgb(220, 220, 220)
    $AllowButton.ForeColor = [System.Drawing.Color]::Green
    $AllowButton.Visible = $False

    $AllowButton.Add_Click({
        $SelectedEntry = $ListView.SelectedItems
        if ($SelectedEntry.Count -eq 0) {
            return
        }
        foreach ($ListEntry in $SelectedEntry) {
            $SelectedFile = $ListEntry.SubItems[3].Text
            Remove-FirewallRule -ExecutablePath $SelectedFile
            Add-FirewallRule -ExecutablePath $SelectedFile -Action "Allow"
            $ListEntry.SubItems[1].Text = "Allowed"
            $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Green
        }
    })

    $BlockButton = New-Object System.Windows.Forms.Button
    $BlockButton.Text = "Block"
    $BlockButton.Location = New-Object System.Drawing.Point(85, 290)
    $BlockButton.Size = New-Object System.Drawing.Size(75, 25)
    $BlockButton.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
    $BlockButton.FlatAppearance.BorderSize = 1
    $BlockButton.BackColor = [System.Drawing.Color]::FromArgb(220, 220, 220)
    $BlockButton.ForeColor = [System.Drawing.Color]::Red
    $BlockButton.Visible = $False

    $BlockButton.Add_Click({
        $SelectedEntry = $ListView.SelectedItems
        if ($SelectedEntry.Count -eq 0) {
            return
        }
        foreach ($ListEntry in $SelectedEntry) {
            $SelectedFile = $ListEntry.SubItems[3].Text
            Remove-FirewallRule -ExecutablePath $SelectedFile
            Add-FirewallRule -ExecutablePath $SelectedFile -Action "Block"
            $ListEntry.SubItems[1].Text = "Blocked"
            $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Red
        }
    })

    $RemoveButton = New-Object System.Windows.Forms.Button
    $RemoveButton.Text = "Remove"
    $RemoveButton.Location = New-Object System.Drawing.Point(165, 290)
    $RemoveButton.Size = New-Object System.Drawing.Size(75, 25)
    $RemoveButton.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
    $RemoveButton.FlatAppearance.BorderSize = 1
    $RemoveButton.BackColor = [System.Drawing.Color]::FromArgb(220, 220, 220)
    $RemoveButton.Visible = $False

    $RemoveButton.Add_Click({
        $SelectedEntry = $ListView.SelectedItems
        if ($SelectedEntry.Count -eq 0) {
            return
        }
        foreach ($ListEntry in $SelectedEntry) {
            $SelectedFile = $ListEntry.SubItems[3].Text
            Remove-FirewallRule -ExecutablePath $SelectedFile
            $ListEntry.SubItems[1].Text = "None"
            $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Black
        }
    })

    $RefreshButton = New-Object System.Windows.Forms.Button
    $RefreshButton.Text = "Refresh"
    $RefreshButton.Location = New-Object System.Drawing.Point(245, 290)
    $RefreshButton.Size = New-Object System.Drawing.Size(75, 25)
    $RefreshButton.FlatStyle = [System.Windows.Forms.FlatStyle]::Flat
    $RefreshButton.FlatAppearance.BorderSize = 1
    $RefreshButton.BackColor = [System.Drawing.Color]::FromArgb(220, 220, 220)
    $RefreshButton.Visible = $False

    $RefreshButton.Add_Click({
        Refresh-ListView
    })

    $ContextMenu = New-Object System.Windows.Forms.ContextMenuStrip
    $AllowMenuItem = New-Object System.Windows.Forms.ToolStripMenuItem
    $AllowMenuItem.Text = "Allow"
    $AllowMenuItem.Image = [System.Drawing.SystemIcons]::Shield.ToBitmap()
    $ContextMenu.Items.Add($AllowMenuItem)
    $ListView.ContextMenuStrip = $ContextMenu

    $AllowMenuItem.Add_Click({
        $SelectedEntries = $ListView.SelectedItems
        if ($SelectedEntries.Count -gt 0) {
            foreach ($ListEntry in $SelectedEntries) {
                $SelectedFile = $ListEntry.SubItems[3].Text
                Remove-FirewallRule -ExecutablePath $SelectedFile
                Add-FirewallRule -ExecutablePath $SelectedFile -Action "Allow"
                $ListEntry.SubItems[1].Text = "Allowed"
                $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Green
            }
        }
    })

    $BlockMenuItem = New-Object System.Windows.Forms.ToolStripMenuItem
    $BlockMenuItem.Text = "Block"
    $BlockMenuItem.Image = [System.Drawing.SystemIcons]::Error.ToBitmap()
    $ContextMenu.Items.Add($BlockMenuItem)

    $BlockMenuItem.Add_Click({
        $SelectedEntries = $ListView.SelectedItems
        if ($SelectedEntries.Count -gt 0) {
            foreach ($ListEntry in $SelectedEntries) {
                $SelectedFile = $ListEntry.SubItems[3].Text
                Remove-FirewallRule -ExecutablePath $SelectedFile
                Add-FirewallRule -ExecutablePath $SelectedFile -Action "Block"
                $ListEntry.SubItems[1].Text = "Blocked"
                $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Red
            }
        }
    })

    $RemoveMenuItem = New-Object System.Windows.Forms.ToolStripMenuItem
    $RemoveMenuItem.Text = "Remove"
    $RemoveMenuItem.Image = [System.Drawing.SystemIcons]::Warning.ToBitmap()
    $ContextMenu.Items.Add($RemoveMenuItem)

    $RemoveMenuItem.Add_Click({
        $SelectedEntries = $ListView.SelectedItems
        if ($SelectedEntries.Count -gt 0) {
            foreach ($ListEntry in $SelectedEntries) {
                $SelectedFile = $ListEntry.SubItems[3].Text
                Remove-FirewallRule -ExecutablePath $SelectedFile
                $ListEntry.SubItems[1].Text = "None"
                $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Black
            }
        }
    })

    $BrowseButton = New-Object System.Windows.Forms.PictureBox
    $BrowseButton.Image = Get-Icon -FilePath ([System.IO.Path]::Combine([System.Environment]::SystemDirectory, "shell32.dll")) -Index 22 -Width 25 -Height 25
    $BrowseButton.Location = New-Object System.Drawing.Point(555, 290)
    $BrowseButton.Size = New-Object System.Drawing.Size(25, 25)
    $BrowseButton.Visible = $False

    $BrowseButton.Add_Click({
        $FolderBrowserDialog = New-Object System.Windows.Forms.FolderBrowserDialog
        $FolderBrowserDialog.RootFolder = [System.Environment+SpecialFolder]::MyComputer
        $Result = $FolderBrowserDialog.ShowDialog()
        if ($Result -eq [System.Windows.Forms.DialogResult]::OK) {
            $SelectedPath = $FolderBrowserDialog.SelectedPath
            Set-Location -Path $SelectedPath
            Refresh-ListView
        }
    })

    $RefreshTimer = New-Object System.Windows.Forms.Timer
    $RefreshTimer.Interval = 250

    $RefreshTimer.Add_Tick({
        $RefreshTimer.Stop()
        Refresh-ListView
    })

    $MainForm.Add_Shown({

        Create-TrayIcon

        $AllowButton.Visible = $True
        $BlockButton.Visible = $True
        $RemoveButton.Visible = $True
        $RefreshButton.Visible = $True
        $BrowseButton.Visible = $True

        $RefreshTimer.Start()

    })

    $MainForm.Controls.Add($ListView)
    $MainForm.Controls.Add($AllowButton)
    $MainForm.Controls.Add($BlockButton)
    $MainForm.Controls.Add($RemoveButton)
    $MainForm.Controls.Add($RefreshButton)
    $MainForm.Controls.Add($BrowseButton)
    $MainForm.ShowDialog()

}

Add-Type -TypeDefinition 'using System; using System.Drawing; using System.Runtime.InteropServices; namespace System { public class IconExtractor { public static Icon Extract(string file, int number, bool largeIcon) { IntPtr large; IntPtr small; ExtractIconEx(file, number, out large, out small, 1); try { return Icon.FromHandle(largeIcon ? large : small); } catch { return null; } } [DllImport("Shell32.dll", EntryPoint = "ExtractIconExW", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)] private static extern int ExtractIconEx(string sFile, int iIndex, out IntPtr piLargeVersion, out IntPtr piSmallVersion, int amountIcons); } }' -ReferencedAssemblies System.Drawing

function Get-Icon {

    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $True)]
        [String]$FilePath,
        [Parameter(Mandatory = $True)]
        [Int32]$IndexNo,
        [Parameter(Mandatory = $False)]
        [Int32]$Width,
        [Parameter(Mandatory = $False)]
        [Int32]$Height
    )

    $IconExtract = [System.IconExtractor]::Extract($FilePath, $IndexNo, $True)

    if (($IconExtract -ne $null) -and ($Width -and $Height)) {
        $Bitmap = New-Object System.Drawing.Bitmap $Width, $Height
        $Graphics = [System.Drawing.Graphics]::FromImage($Bitmap)
        $Graphics.DrawImage($IconExtract.ToBitmap(), 0, 0, $Width, $Height)
        $Graphics.Dispose()
        $IconExtract.Dispose()
        return $Bitmap
    } else {
        return $null
    }

}

function Get-ExecutableStatus {

    param (
        [String]$ExecutablePath
    )

    $FirewallRule = Get-NetFirewallApplicationFilter -Program $ExecutablePath | Get-NetFirewallRule

    $InboundBlocked = $FirewallRule | Where-Object { $_.Direction -eq "Inbound" -and $_.Action -eq "Block" }
    $OutboundBlocked = $FirewallRule | Where-Object { $_.Direction -eq "Outbound" -and $_.Action -eq "Block" }
    $InboundAllowed = $FirewallRule | Where-Object { $_.Direction -eq "Inbound" -and $_.Action -eq "Allow" }
    $OutboundAllowed = $FirewallRule | Where-Object { $_.Direction -eq "Outbound" -and $_.Action -eq "Allow" }

    if ($InboundBlocked -and $OutboundBlocked) {
        return "Blocked"
    } elseif ($InboundAllowed -and $OutboundAllowed) {
        return "Allowed"
    } elseif (($InboundBlocked -and $OutboundAllowed) -or ($InboundAllowed -and $OutboundBlocked)) {
        return "Mixed"
    } elseif ($InboundAllowed -and (-not $OutboundAllowed)) {
        return "Mixed"
    } elseif ($OutboundAllowed -and (-not $InboundAllowed)) {
        return "Mixed"
    } elseif ($InboundBlocked -and (-not $OutboundBlocked)) {
        return "Mixed"
    } elseif ($OutboundBlocked -and (-not $InboundBlocked)) {
        return "Mixed"
    } else {
        return "None"
    }

}

function Remove-FirewallRule {

    param (
        [String]$ExecutablePath
    )

    try {
        $ExistingRule = Get-NetFirewallApplicationFilter -Program $ExecutablePath
    } catch {
        $ExistingRule = $null
    }
    if ($ExistingRule -ne $null) {
        $ExistingRule | Remove-NetFirewallRule
    }

}

function Add-FirewallRule {

    param (
        [String]$ExecutablePath,
        [String]$Action
    )

    $ExecutableName = [System.IO.Path]::GetFileName($ExecutablePath)

    if ($Action -eq "Block") {

        New-NetFirewallRule -DisplayName "Blocked Inbound Executable ($ExecutableName)" -Enabled "True" -Action "Block" -Direction "Inbound" -Protocol "Any" -Profile @("Domain", "Private", "Public") -Program $ExecutablePath
        New-NetFirewallRule -DisplayName "Blocked Outbound Executable ($ExecutableName)" -Enabled "True" -Action "Block" -Direction "Outbound" -Protocol "Any" -Profile @("Domain", "Private", "Public") -Program $ExecutablePath

    } elseif ($Action -eq "Allow") {

        New-NetFirewallRule -DisplayName "Allowed Inbound Executable ($ExecutableName)" -Enabled "True" -Action "Allow" -Direction "Inbound" -Protocol "Any" -Profile @("Domain", "Private", "Public") -Program $ExecutablePath
        New-NetFirewallRule -DisplayName "Allowed Outbound Executable ($ExecutableName)" -Enabled "True" -Action "Allow" -Direction "Outbound" -Protocol "Any" -Profile @("Domain", "Private", "Public") -Program $ExecutablePath
    }

}

function Refresh-ListView {

    $ListView.Items.Clear()
    $MainForm.Text = "PowerShell: Firewall Control (Loading File List)"
    $Executables = Get-ChildItem -Recurse -File -Filter *.exe

    $FullPathColumnWidth = $ListView.Columns[3].Width
    $HighestValue = 0
    $BoostValue = 5

    foreach ($Executable in $Executables) {

        $FileName = $Executable.Name
        $FullPath = $Executable.FullName

        $FirewallStatus = Get-ExecutableStatus -ExecutablePath $FullPath
        $ExecutableIcon = [System.Drawing.Icon]::ExtractAssociatedIcon($FullPath)

        $StatusColumnWidth = $ListView.Columns[1].Width
        $FilePathColumnWidth = $ListView.Columns[2].Width
        $CurrentValue = [System.Windows.Forms.TextRenderer]::MeasureText($FullPath, $ListView.Font).Width

        if ($CurrentValue -gt ($HighestValue + $BoostValue)) {
            $HighestValue = $CurrentValue + $BoostValue
        }

        if ($FirewallStatus -eq "Allowed") {
            $FirewallStatusColor = [System.Drawing.Color]::Green
        } elseif ($FirewallStatus -eq "Blocked") {
            $FirewallStatusColor = [System.Drawing.Color]::Red
        } elseif ($FirewallStatus -eq "Mixed") {
            $FirewallStatusColor = [System.Drawing.Color]::Orange
        } else {
            $FirewallStatusColor = [System.Drawing.Color]::Black
        }

        $ListEntry = New-Object System.Windows.Forms.ListViewItem("")
        $ListEntry.ImageIndex = $ListView.SmallImageList.Images.Add($ExecutableIcon, [System.Drawing.Color]::Transparent)
        $ListEntry.SubItems.Add($FirewallStatus)
        $ListEntry.SubItems.Add($FileName)
        $ListEntry.SubItems.Add($FullPath)
        $ListEntry.UseItemStyleForSubItems = $False
        $ListEntry.SubItems[1].ForeColor = $FirewallStatusColor

        $ListView.Items.Add($ListEntry)

    }

    if ($HighestValue -gt $FullPathColumnWidth) {

        $ListView.Columns[3].Width = $HighestValue

    }

    $MainForm.Text = "PowerShell: Firewall Control"

}

function Cycle-Status {

    param (
        [System.Windows.Forms.ListViewItem]$ListEntry
    )

    $Status = $ListEntry.SubItems[1].Text
    $SelectedFile = $ListEntry.SubItems[3].Text

    Remove-FirewallRule -ExecutablePath $SelectedFile

    if ($Status -eq "None") {
        $Status = "Allowed"
        $Action = "Allow"
    } elseif ($Status -eq "Allowed") {
        $Status = "Blocked"
        $Action = "Block"
    } elseif ($Status -eq "Blocked") {
        $Status = "None"
    }

    if ($Status -eq "Allowed" -or $Status -eq "Blocked") {
        Add-FirewallRule -ExecutablePath $SelectedFile -Action $Action
    }

    $ListEntry.SubItems[1].Text = $Status

    if ($Status -eq "Allowed") {
        $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Green
    } elseif ($Status -eq "Blocked") {
        $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Red
    } else {
        $ListEntry.SubItems[1].ForeColor = [System.Drawing.Color]::Black
    }

}

function Create-TrayIcon {

    $TrayContextMenu = New-Object System.Windows.Forms.ContextMenu
    $MenuItemExit = New-Object System.Windows.Forms.MenuItem
    $MenuItemExit.Text = "Exit"

    $MenuItemExit.Add_Click({

        $TrayIcon.Dispose()
        $MainForm.Dispose()
        [System.Windows.Forms.Application]::Exit()
        Stop-Process -Id $PID

    })

    $TrayContextMenu.MenuItems.Add($MenuItemExit)

    $TrayIcon = New-Object System.Windows.Forms.NotifyIcon
    $TrayIcon.Text = "PowerShell: Firewall Control"
    $TrayIcon.Icon = [System.Convert]::FromBase64String("AAABAAMAEBAAAAEAIABoBAAANgAAABgYAAABACAAiAkAAJ4EAAAgIAAAAQAgAKgQAAAmDgAAKAAAABAAAAAgAAAAAQAgAAAAAAAABAAA6wAAAOsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABTVV0AU1VeBVRVXAVTVV0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIyTOCCEhzgojIc8JJSPOByQhzgoQEOUJXV14IWBgZYtfXmOLWFZ2IyEc3Q0nJtUOJybVCCck0AkiIM4KJSLOCEVE7Kk2OeuvLTLrp0FB7Js4OuyuQETXrIB/mMuvoZX9r6CU/Xx6lNA6MsK5KCLauDEv5aFAP+uoMzfrrysx7KloaPv1T1b6+TtG+fxYWvn2WF31/4aKzf/Sw7f/9dSx//PQqv/TwLH/fHe3/zIr0/85N+v5V1f3/UpS+vk1RPv1gH3/Qlxc95BHSvP5PULz/3B13v/OyMj/wcbO/4KR0v+Fis//wrzC/87Ewf9hXMX/Mi3i/zc67/k/R/eQR1T/QREIzhJlYfV3Xl/5/VNb9f+mp9D/xtbW/0my9P8ph/f/KHb1/z6V7f/Dysf/op3C/0E82/82OvH9LTn0dyAUzxJQT/HeQUXx6zpA8/1sb+b/ysHE/8Xh3/9T1Pv/Psb9/y+2/f8trvf/scXF/8u+v/9VUM//Ozfp/jg88OssNvHecnH901Zc++U/Sfn8honj/9fJvf/169r/jeXy/1Ph//9F1v//b8Pf/+vRsv/g0ML/a2jJ/0E75/1JTvjnOkn9052X/w5iYPd3UFP3/YeK3v/ezbv//vDe/9Lt6P915fj/aNnx/77Vz//+4MD/59fF/3Rwxf8sKeD/Mzjxelpq/w42M95aUlHunVxe+PmSlN7/3Maw//vo0///8eD/1O3n/9Pn3f//6c///+fK/+bYx/94c8T/NDLi+jA27J4oKN9aWlr2/UdN9/86RPn8h4rp9cC3u//bxbL/7ta9//rlzv//7Nj/+efU/+bZzP/Bvsn/c3TX9VNU9fxCSff/MDz2/XZ1/ZZdYv2bRlH9lHV2+4ebnuibtrfNucW9vOrVxLX+2czB/sjFyey1t8q9hYvcnFti9odwcP6UWF/9mz5M/ZaelPoDhH34BGxr+ANrY/4CNTT/BKKr7w6/xdY6vsHMhry/yYi3vMo+oaTUDyou/gRDS/8CnIr4A354+ARtdPoDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyNPuAMjT7wLH0+oCyNPrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//wAA/n8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP5/AAD//wAAKAAAABgAAAAwAAAAAQAgAAAAAAAACQAA6wAAAOsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABeXnoAYmJrBWNjaQVnZ1sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAg4OFAAAAAABdXGAiV1dce1ZWW3taWFwiAAAAAHx9fQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADY23C8vL9o7KCraOyco2jwsLN4qNTTbMS8u2jwoKNo7KCrbOWBglU1qanHBdHFz/nRxc/5oZ2/BWFWSVi4nz0spJdNLJyXXRyko2zc0Mt4rMTDaPCoq2jsnKdo7KSncL0hF7NU9PezdMzbs3Sow7N41N+zERELszzs87N4xNOzdRUjU3H1+meWdmJj/17+o/9e/p/+emJj/d3aU6j83vecpH9LnKiTd5Swp5NU+O+rJPz7r3jU37N0sMezdKjDs1Wtq/P9bX/z/SVP8/zZF/P9GUPztZ2j89Fdd/f9VXe//h4u//7m0tP/r0rn//tmw//vTqP/pzK//u7W1/357rP86MMP/LCbc/zEy6/hTUvXzXmH7/01W/P87SPz/Lz/8/3x4/bNsbP27U1b31z5F8/ZCR/T/U1P0/1ZY8v+KjtX/wcDF/+/axP/52LX/0r2x/9K8sP/20Kj/79S5/8PAxf9ybrj/MyvS/zAt5P9DQe3/UE/y909U99dLVf27Okn9s4aB/Bh4df0cWFf0oUtN8/o+Q/P/Mjrz/1lf4v+ztMf/5NfK/8nMzv9vltv/VXDh/1lp4f90idf/yMG9/+jUwv+ursL/UErE/zAq3/8xMOr/LTPw+y869KFXX/0cSFT8GAAAAAAAAP8AeHX9mGlr/f5WXf3/Slb6/4+W1f/Oycn/09rW/06v8P8nkfn/KnX0/y5r8/8qcvT/Rprq/9HMvv/Uy8f/hYO+/z022v87O+3/NT/3/y1A/JjENJ0AAAAAADkz4XUyLuF5TUnrxFFP7/xMTPH/YWTp/7KzyP/j1sr/pdro/zzE/f82vv3/LqP6/yeX+f8gmvr/HaD6/5G90P/o07//q6vA/0tDzv83MeP/Nzfr/TA068QpJ+F5KSjidFlZ9f9LT/X/PEP1/i469P5AR/b/hYfh/766wf/w3cn/vOXn/1vX+v9M2f7/Qsz+/zbB/f8su/7/La/2/5u+yf/x0rP/wr/F/2RgxP84MeH/QT7s/j1B8v4xPPX/Kzj1/3Z0/fdlaP38Ulr9+T1L+/xOVvv/oaTd/8W9uf/34Mj/9u7e/5jk7/9V4f7/VeD+/0zY/v88yv7/eb7X/+fLq//52LT/083L/317v/87NN//TEft/U5R+PpDT/38M0T994F8/GZwcP1uVFb0wUNG8vpHSvH/oaPW/8/BtP/44sv///Hg/8Hr6f9i4/z/WOP+/1Tf/v9Azf7/q8fD//nXtf/+377/29LK/4uKvv8zK9b/ODPl/EFB8MdOVvxvP0z8ZgAAAAAAAP8AZ2X5m1hb+f9RWPf/oqbW/9TDsv/45M7///Pl//Hv4v+76ur/Z+T7/2La8/+g09j/5NzI//7jxf/+4sL/3tPJ/5GQv/8yKtH/Kirk/yoy8aIS//8AAAAAADIt1CotKNQvbWj1pWpp+vpkafj/qKvW/9TAq//138b//vPk///z4//68OD/rent/6zk5P/658///+jO//7myv/+5Mb/3tTK/5ORv/81LMz/NDXo+i8786YoJNQvJyTUKkZD6dE7O+jaP0Ht5UNG8vdUVvL/qazY/8m3qP/qyaj/9N3E//rp1v/9797/+/Dg//zu3P//7Nf//+rT///ozv/548r/1M7K/5GRwP8+ONj/QkTw9zc67uUsMOjaKi7p0Wdn+/9YXPv/Rk/7/zNB+/9HT/vsn6Hp9Le3xv/Ct7L/2MGs/+jLrf/z2b7/+eTN//7s2P/+7dn/+OfV/+TYzv/MyMn/s7TH/3x/0vRaW/XsXF/7/0pS+/86Rvv/Lj37/3x4/chrbf3PWV/9z0RR/dBRWf6wfnz7vZWY7s+yttrUuLnH67+5uv7Tv67/48et/+jSvP/az8j/xMLI/ri6x++oq8zWeoDhz0xX971ubv6wcHD90Fxi/c9KVP3POUj9yIaA/Rt5d/siZ2j7IlNb+yJYW/wXgn38HHNx/CKAh/QrsbfiU8HF05G7vcnWwb3A/cG+wf65u8fcubzIl6+xz1qIiecuV1z9IkpV/Rx5dvwXf3f7Imtr+yJXXvsiRlT9GwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/yP8AydHbAM3R3QfIztsvv8TTZr7D0GbAxNIyvcTQCcK80AC20tEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADk2P8A29zwAtnd6QLJ5KMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wD///8A/+f/AP/D/wAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAwAAAAAAAAAAAAAAAAAAAAAAwAADAAAAAAAAAAAAAAAAAAAAAAAAAAAA/4H/AP/n/wD///8A////ACgAAAAgAAAAQAAAAAEAIAAAAAAAABAAAOsAAADrAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQEBABF1dZHBdXWJwQEBABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHZ2fCldXGHkVlZa/1VVWf9bWV7kcHB2KQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA5Od5VMjLadi0t2nYnKdp2Jynadigo3HM2NuI0NjTadjAw2nYrKdp2Jynadicp2nZbW6twa2t093R1fP+ZkpD/mZOQ/3V1fP9mZXD5U0+mhC4nz5sqJdCZKCbVkicl2IspJ9qELjLkODcz3HQwMNp2Kyvadicp2nYnKdp2KireVUlE7fxAP+3/OTnt/zE07f8qL+3/Ki/t/0I+69pDQe3/PDvt/zQ27f8tMO3/RkrW/4KCj/6PkZn/vrKp//vYsv/617L/wbSp/5GTnP96eYb/QTi9/ygbzv8qINj/KiPf/yon5f81MefjQ0Ds/z087f82N+3/LjLt/yov7f8rMO38bm79/2Fk/f9TW/3/RVH9/zdH/f8rPv3/ZGf+x2Zo/f9YX/3/S1X9/01Z7v+Rkqf/paey/9XGuv/+3Lj//dqy//jRqf/506r/2Me4/6mrt/+GhJb/OC68/ysh0f8uKt//Ky7q/0RD89hhYfr/W2H9/01X/f9ATf3/MkP9/ys+/f98eP3/bm79/2Fk/f9TW/3/RVH9/zdH/f9kZv3/dHL9/2Zo/f9bYfv/mZ3C/7W5w//bz8f//t++//3cuf/zzaP/9M2j//fQpv/61Kz/39LH/7q9yP+Fg6X/LSLJ/zAp2/8xMOj/QUPy/2pk9/9pav3/W2H9/01X/f9ATf3/MkP9/4aB/GF8eP13bW39d0pI7L49POjrNznq/zY36v84Oer/Njjq/3+C1f+2ucP/0czM//7jxP/83r3/68ah/5yhw/+coMX/78yl//jRqP/71q//29XV/7Gzvv9mYLP/KyLU/y0m3f8tKuP/MzHo/zU06Ow8Puy+WmD9d01Y/XdATfxgAAAAAAAAAAAAAAAAW1r31VBS9v9FSvb/OkL2/zA69v89SO//srTD/7/Byf/348r/6NjI/1WU4v8wbfL/O1bt/z1R7P88Ve3/VIzj/+nNsf/21rT/ztDd/5+grP88Msv/Mizf/zMx6P8wM+//KzT0/yo499UAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB2c/3MaGn9/1pg/f9MVv3/Pkz9/4SM2v+3usX/3dTM//Hj0P9Ep+//Jpj6/yp+9f8vbvH/Mmjx/zNn8f8tcfL/Op3v//DRrv/l1Mb/ur7J/3Jutv83L9z/Ozjo/zk+8/8yPvj/Kz/9zAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIN9/MR1c/3/aGn9/1pg/f9TXfn/tLjJ/7q7w//76NP/mdPn/y63/f8vuP3/KKT6/yiL9/8og/b/I4n4/yCL+P8Xnfz/i7rS//nWsP/Nz9v/nZ6s/z4z1f8/OeX/QEDw/z1E9v8yQ/vFAAAAAAAAAAAAAAAAOzXh5jUv4fIvLOHyLSrh8y4s4fkvLOL/QD3o/2lo2v+3usb/1Me9///s1/+B2PD/P8z+/0XS/v89yv7/MbD7/yqm+/8ir/z/HaX8/xWl/v9ftOH/+NKq/97Syf+xtL//U0zB/y0k2v8xKd7/MSri+y8r4vQpJ+HyKSfh8iko4uVnaPz/Wl/8/0xV/P9AS/z/MkL8/ys9/P9eYPz/naHe/7S3w//kzrf//+7a/7Xk6P9Y1fr/Ut7+/0zY/v9Dz/7/O8j+/zTD/v8ruv7/J6z3/4W60f/20an/7NO5/77Bzf93c7f/Mize/0U/6v9HR/L/Q0n3/zpH/P8sPvz/Kz38/3d0/f9pa/3/W2H9/01X/f9ATf3/MkP9/2dp/f+1uNb/sbK7/+7Rsv//793//+7a/6Dj7P9U3/7/WuT+/1Pe/v9L1/7/Q9D+/zTD/v9/u9L/8suj//bRq//627n/xsrX/5GQsP8xKdr/Rz/m/05L8f9OUPf/SFL9/zpJ/f8sP/3/hX78xXd1/d1pa/3dV1344UhO9PFASPX/YWH0/7/C0P+7tbb/8NKz///x4P//793/0+zk/1bh/v9W4f7/WuT+/1rk/v9I1P7/Ocf+/7/Cs//yzKf//d68//7eu//NztX/paex/zAo0/9BOOD/SEHp91FP8+hTWfrfSFP93TtJ/MQAAAAAAAAAAAAAAABMSe7YQkHv/zo87/9BRO3/v8LP/8W6sv/w07X///Lj///x4P/j7eP/hOf1/1nh/f9Y4/7/WOL+/0TQ/v87yf7/4sen//zhwf/+4cH//t++/9LO0f+rrbf/NCnL/yki2v8qJ+P/Kyzs4QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHFv/cxiZv3/VFz9/1pk9/+/ws//ybuw//DUtv//9Ob///Lj//rw4f/8797/qOnt/1nj/v9U3v7/msnH/9/fzf/248n//uTH//7jxP/+4cH/1M7P/66wu/88Msf/LSjb/yss5/8rNvPYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAfnn9zHBv/f9iZv3/Zm73/8DD0P/Kuar/7tCw///26f//9Ob///Lj///x4P/6797/deX4/3bg7//55cz//+nQ//7nzf/+5sr//uTH//7jxP/Tzs7/sLG8/zswwf8uJtf/NDbr/y5A/cwAAAAAAAAAAAAAAAA4M9mcMi7Yti0q2LZRTujPX1zw7FlX8f9sbvH/wMTQ/8m2p//pwpz/9uDJ//rr2f//9Ob///Lj///x4P/17t//9u7c///s1///69T//+nQ//7nzf/+5sr//uTH/9DMzf+wsr3/OzDB/zYw2/9BRfDsMzfozykm2LYpJti2JybZnFJQ9P9JSvT/P0P0/zU89P8sNfT/KzT0/15e8v/Gytf/raqu/+C8mP/qxqP/8NK0//XdxP/559P//vDg///x4P//793//+7a///s1///69T//+nQ//7nzf/q2sr/wcLM/6+xvP87NdD/TUzz/0VH9P88QPT/Mjn0/ys09P8rNPT/cXH9/2Rn/f9WXf3/SFP9/zpJ/f8tP/3/aGr+x6uv6P+/w8//ra+5/7uzsf/bwaj/6MSg/+7Pr//z2r7/+OPN//3t2v//793//+7a//3r1v/l2c//xsXK/7m8x/+0tsH/g4TI/1RZ/Mhsbf3/XmP9/1BZ/f9DT/3/NUX9/ys+/f+Aev36cXH9/2Rn/f9WXf3/SFP9/zpJ/f9zcf69eHX9/42P8v+zuOP/wsbS/7S2wP+xr7T/1cCu/+fFov/ty6r/8ta5//Ldx//Z0Mr/urvE/7a5xf+2ucP/oaTO/3B35/8+S/z/XGL+vXp2/f9sbf3/XmP9/1BZ/f9DT/3/NUX9+oiC/y+AfPtEcXH7RGVl+0RWXvtESVH7Qntx/xuDgPtEeHT7RGlt+0SPl+5ptbvgtMTI1fm3usb/r7C5/829sf/Mv7b/s7O9/7a5xP+6vMj7srTNwZyc3HZlaftEWl77REtW+0RVXv8bh4D7Qnx0+0RtbftEXmL7RE9a+0RBUf8vAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAyMjeF8fM2HfFydfju7/L/7u/yv/AxNDpvcHOiLnByh0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALbb2wfFyNdGxMjTRba22wcAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA///////////////////////8P///+B//AAAAAAAAAAAAAAAAAAAAAAAAAADgAAAH4AAAB+AAAAcAAAAAAAAAAAAAAAAAAAAA4AAAB+AAAAfgAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAD/8A////w///////////////////////8=")
    $TrayIcon.Visible = $True
    $TrayIcon.ContextMenu = $TrayContextMenu

}

Create-FirewallGUI
Stop-Process -Id $PID